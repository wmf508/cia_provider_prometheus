dev:
	go run ./cmd/assets-collector/main.go -dev
test:
	@go test -cover ./...
update:
	go get -u gitlab.com/wmf508/cia
	go get -u gitlab.com/wmf508/alfred/database/mongodb
	go get -u gitlab.com/wmf508/alfredy/kubernetes/node
.PHONY: test dev