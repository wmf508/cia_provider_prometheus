package node

import (
	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
)

type NodeProvider struct {
	Api v1.API
}

type PromNode struct {
	Node       string `json:"node"`
	InternalIp string `json:"internal_ip"`
	Status     string `json:"status"`
}
