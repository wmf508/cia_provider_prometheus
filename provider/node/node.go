package node

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	commonlog "gitlab.com/wmf508/alfredy/log"

	"github.com/prometheus/common/model"
	commonNode "gitlab.com/wmf508/alfredy/kubernetes/node"
	commonNodeProv "gitlab.com/wmf508/cia/provider/node"
	"gitlab.com/wmf508/cia_provider_prometheus/internal/api"
	publicProv "gitlab.com/wmf508/cia_provider_prometheus/provider"
)

var log = commonlog.Logger

// TODO: add log warning setting to config
var logWarnings = true

var nodeStatusMap = map[string]int{
	"true":    1,
	"false":   2,
	"unknown": 3,
}

func NewNodeProvider(config publicProv.ProviderConfig) (commonNodeProv.NodeProvider, error) {
	api, err := api.NewApi(config)
	if err != nil {
		return nil, err
	}

	nodeProv := &NodeProvider{
		Api: api,
	}

	return nodeProv, nil
}

// get nodes infomation from prometheus server
func (cli *NodeProvider) GetNodes() ([]*commonNode.Node, error) {

	// 获取kube_node_status_condition指标中Ready状态相关的瞬时向量样本，并从kube_node_info中获取internal_ip信息
	value, warnings, err := cli.Api.Query(context.TODO(), "kube_node_status_condition{condition=\"Ready\"} > 0 or kube_node_info", time.Now())
	if warnings != nil && logWarnings {
		log.Printf("prometheus api warning: %v", warnings)
	}

	if err != nil {
		return nil, err
	}

	nodes := []*commonNode.Node{}

	// 当前查询返回的是Vector类型数据
	vector, _ := value.(model.Vector)
	for _, sample := range vector {
		node, exist, err := unmarshalNodeFromPrometheus(sample, nodes)
		if err != nil {
			return nil, fmt.Errorf("prometheus provider: %w", err)
		}

		// 没有解析过则新添加一条记录
		if !exist {
			nodes = append(nodes, node)
		}
	}

	return nodes, nil
}

// 将从prometheus接口获取的节点数据转化成结构体
func unmarshalNodeFromPrometheus(sample *model.Sample, nodes []*commonNode.Node) (*commonNode.Node, bool, error) {
	promNode := &PromNode{}
	node := &commonNode.Node{}
	exist := false
	var existNode *commonNode.Node

	nodeValue, err := json.Marshal(sample.Metric)
	if err != nil {
		return nil, false, fmt.Errorf("prometheus provider: %w", err)
	}

	// kube_node_status_condition主要用来收集节点的主机名、ready状态
	if sample.Metric["__name__"] == "kube_node_status_condition" {
		err = json.Unmarshal(nodeValue, &promNode)
		if err != nil {
			return nil, false, fmt.Errorf("prometheus provider: %w", err)
		}

		// 是否已经通过其他指标解析过
		existNode, exist = nodeIsUnmarshaled(promNode, nodes)

		// get node ready status value
		status := nodeStatusMap[promNode.Status]

		if exist {
			// 如果节点已经被其他指标解析过,此处只需要赋值Ready状态即可
			existNode.Status = status
		} else {
			node.Hostname = promNode.Node
			node.Status = status
		}

	}

	// kube_node_info主要用来收集节点的主机名、internal ip
	if sample.Metric["__name__"] == "kube_node_info" {
		err = json.Unmarshal(nodeValue, &promNode)
		if err != nil {
			return nil, false, fmt.Errorf("prometheus provider: %w", err)
		}

		// 是否已经通过其他指标解析过
		existNode, exist = nodeIsUnmarshaled(promNode, nodes)

		if exist {
			// 如果节点已经被其他指标解析过,此处只需要赋值internal ip即可
			existNode.InternalIp = promNode.InternalIp
		} else {
			node.Hostname = promNode.Node
			node.InternalIp = promNode.InternalIp
		}
	}
	return node, exist, nil
}

// 节点是否已经通过其他指标解析过
func nodeIsUnmarshaled(node *PromNode, nodes []*commonNode.Node) (*commonNode.Node, bool) {
	for _, no := range nodes {
		if node.Node == no.Hostname {
			return no, true
		}
	}
	return nil, false
}
