package node

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/wmf508/cia_provider_prometheus/internal/test"
)

func TestGetNodes(t *testing.T) {

	prov, err := NewNodeProvider(test.TestProviderConfig)
	require.NoError(t, err)
	require.NotEmpty(t, prov)

	nodes, err := prov.GetNodes()
	require.NoError(t, err)
	require.NotEmpty(t, nodes)
}
