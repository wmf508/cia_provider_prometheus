package provider

type ProviderConfig struct {
	Address  string
	Port     int
	Protocol string
}
