package api

import (
	"log"

	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
	monitorProm "gitlab.com/wmf508/arksec_common/monitor/prometheus"
	publicProv "gitlab.com/wmf508/cia_provider_prometheus/provider"
)

// create prometheus api instance
func NewApi(config publicProv.ProviderConfig) (v1.API, error) {

	promConfig := monitorProm.Config{
		Address:  config.Address,
		Port:     config.Port,
		Protocol: config.Protocol,
	}

	api, err := monitorProm.GetApi(promConfig)
	if err != nil {
		log.Fatal("get prometheus api failed!")
	}

	return api, nil
}
