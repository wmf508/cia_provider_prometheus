package api

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/wmf508/cia_provider_prometheus/internal/test"
)

func TestNewProvider(t *testing.T) {

	api, err := NewApi(test.TestProviderConfig)
	require.NoError(t, err)
	require.NotEmpty(t, api)
}
