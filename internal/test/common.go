package test

import publicProv "gitlab.com/wmf508/cia_provider_prometheus/provider"

var TestProviderConfig = publicProv.ProviderConfig{
	Address:  "192.168.77.53",
	Port:     32101,
	Protocol: "http",
}
